module.exports = {
  important: true,
  theme: {
    color: {
      primary: '#0052d9',
    },
    maxWidth: {
      80: '80%',
    },
  },
  purge: [
    './src/**/*.tsx',
  ],
  variants: {},
  plugins: [],
}
